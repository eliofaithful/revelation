# -*- encoding: utf-8 -*-
from django import template

register = template.Library()


@register.filter("stripper")
def stripper(value):
    """stripper."""
    if value:
        return str(value).strip()
    else:
        return value
