# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from revelation.views import (
    RevelationCreateView,
    RevelationUpdateView,
    RevelationDeleteView,
    RevelationEngageView,
    RevelationListView,
    RevelationManyView,
    RevelationOptimizeView,
    RevelationPageView,
    RevelationHomeView,
)

urlpatterns = [
    path(
        "cr/<str:page_slug>/<str:role>/<str:thing>",
        RevelationCreateView.as_view(),
        name="create.thing",
    ),
    path(
        "u/<str:page_slug>/<str:role>/<str:thing>/<int:pk>",
        RevelationUpdateView.as_view(),
        name="update.thing",
    ),
    path(
        "d/<str:page_slug>/<str:thing>/<int:pk>",
        RevelationDeleteView.as_view(),
        name="delete.thing",
    ),
    path(
        "e/<str:page_slug>/<str:thing>/<int:pk>",
        RevelationEngageView.as_view(),
        name="engage.thing",
    ),
    path(
        "l/<str:page_slug>/<str:thing>",
        RevelationListView.as_view(),
        name="list.things",
    ),
    path(
        "m/<str:page_slug>/<str:thing>/<int:pk>/<str:prop>",
        RevelationManyView.as_view(),
        name="many.things",
    ),
    path(
        "o/<str:page_slug>/<str:thing>",
        RevelationOptimizeView.as_view(),
        name="optimize.things",
    ),
    path("p/<str:page_slug>", RevelationPageView.as_view(), name="engage.page"),
    path("", RevelationHomeView.as_view(), name="home"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
