# -*- encoding: utf-8 -*-
from django.apps import apps
from django.conf import settings
from django.urls import reverse_lazy
from slugify.slugify import slugify
from dna.views import (
    EveryCreateViewMixin,
    EveryUpdateViewMixin,
    EveryDeleteViewMixin,
    EveryEngageViewMixin,
    EveryListViewMixin,
    EveryManyViewMixin,
    EveryOptimizeViewMixin,
    EveryPageViewMixin,
    EveryHomeViewMixin,
)


class ChaptersMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        Model = apps.get_model(
            model_name="Chapter", app_label=settings.APP_NAME
        )
        chapters = {"page": {"Chapters": {"status": "label", "page": {}}}}
        chaps = chapters["page"]["Chapters"]
        for chap in Model.objects.filter(
            elio_key=slugify("Read-Chapter"),
            elio_page="/p/read",
            elio_role="list",
        ):
            chaps["page"][chap.name] = {
                "url": reverse_lazy(
                    "engage.thing",
                    kwargs={
                        "page_slug": "read",
                        "thing": "Chapter",
                        "pk": chap.pk,
                    },
                )
            }
        context["chapters_menu"] = chapters
        return context


class RevelationCreateView(ChaptersMixin, EveryCreateViewMixin):
    template_name = "revelation/thing_form.html"


class RevelationUpdateView(ChaptersMixin, EveryUpdateViewMixin):
    template_name = "revelation/thing_form.html"


class RevelationDeleteView(ChaptersMixin, EveryDeleteViewMixin):
    template_name = "revelation/thing_delete.html"


class RevelationEngageView(ChaptersMixin, EveryEngageViewMixin):
    template_name = "revelation/thing_engaged.html"


class RevelationListView(ChaptersMixin, EveryListViewMixin):
    template_name = "revelation/thing_listed.html"


class RevelationManyView(ChaptersMixin, EveryManyViewMixin):
    template_name = "revelation/thing_many.html"

    def get_success_url(self):
        return reverse_lazy(
            "engage.page",
            kwargs={
                "page_slug": "read",
                "thing": "Chapter",
                "pk": self.pk,
            },
        )


class RevelationOptimizeView(ChaptersMixin, EveryOptimizeViewMixin):
    template_name = "revelation/thing_optimize.html"


class RevelationPageView(ChaptersMixin, EveryPageViewMixin):
    template_name = "revelation/thing_page.html"


class RevelationHomeView(ChaptersMixin, EveryHomeViewMixin):
    template_name = "revelation/home_page.html"
