from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from slugify.slugify import slugify
from fuster.fuster import Fuster


class Command(BaseCommand):
    help = "Builds the core data from the Book"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str)

    def handle(self, *args, **options):
        path = options["path"]

        html_doc = open(path, "r").read()
        book = Fuster.gut(html_doc)

        Model = apps.get_model(model_name="Book", app_label=settings.APP_NAME)
        Model.objects.all().delete()
        Model(
            name=" ".join(book["title"]["text"]),
            elio_key=slugify("Read-Book"),
            elio_page="/p/read",
            elio_role="engage",
        ).save()
        del book["title"]

        # Book license
        Model = apps.get_model(model_name="Thing", app_label=settings.APP_NAME)
        Model(
            description=" ".join(book["license"]["text"]),
            elio_key=slugify("License-Thing"),
            elio_page="/p/license",
            elio_role="engage",
        ).save()
        del book["license"]

        # Book Chapters
        Model = apps.get_model(
            model_name="Chapter", app_label=settings.APP_NAME
        )
        Model.objects.all().delete()
        for chapter_title, paragraphs in book.items():
            Model(
                name=chapter_title,
                text=" ".join(paragraphs["text"]),
                elio_key=slugify("Read-Chapter"),
                elio_page="/p/read",
                elio_role="list",
            ).save()
