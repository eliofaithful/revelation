from bs4 import BeautifulSoup
import copy
import pprint

pp = pprint.PrettyPrinter(indent=4)


def lister(dick, key):
    if not dick.get(key):
        dick[key] = {"text": []}
    return dick[key]["text"]


class Fuster:
    @staticmethod
    def gut(html_doc):
        soup = BeautifulSoup(html_doc, "html.parser")
        book = {}
        for tag in soup.body.children:
            if isinstance(tag, str):
                pass
            else:
                if tag.name == "h2":
                    cur_chapter = tag.text
                elif tag.name in ["h1"]:
                    lister(book, "title").append(tag.text)
                elif tag.name == "pre":
                    lister(book, "license").append(tag.text)
                elif tag.name in ["div", "hr", "h3"]:
                    pass
                else:
                    lister(book, cur_chapter).append(tag.prettify())

        return book
