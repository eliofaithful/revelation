![](elio-revelation-logo.png)

# revelation ![notstarted](https://elioway.gitlab.io/static/notstarted.png "notstarted")

Convert gutenberg books into a book the elioWay.

## Nutshell

```
django-admin livereload
django-admin runserver
```

## License

[HTML5 Boilerplate](LICENSE.txt) © [Tim Bushell]()

[elioWay](https://gitlab.com/elioway/elio/blob/master/README.md)

![](apple-touch-icon.png)
