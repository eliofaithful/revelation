# Installing frankenstein

- [Prerequisites](./prerequisites)

## Git

.. is also a fine way to install **frankenstein**, using GIT as a starting project.

```bash
cd elioway
git clone https://gitlab.com/elioway/eliofaithful.git
cd eliofaithful
git clone https://gitlab.com/eliofaithful/frankenstein.git
```
