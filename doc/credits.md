# frankenstein Credits

## Artwork

- <https://commons.wikimedia.org/wiki/File:Octicons-flame.svg>
- <https://commons.wikimedia.org/wiki/File:Book_of_Revelation_Chapter_19-2_%28Bible_Illustrations_by_Sweet_Media%29.jpg>

## Core, Thanks

- <https://www.crummy.com/software/BeautifulSoup/bs4/doc/>
