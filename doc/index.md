# frankenstein

<figure>
  <img src="star.png" alt="">
</figure>

> It lives **the elioWay**

![experimental](//docs.elioway/./static/experimental.png "experimental")

This repository is what you would end up with if your followed the **dna** QuickStart, and therefore makes for a useful demostration as well as a boilerplate for a new application. However the recommended way of creating a **dna** driven app the **elioWay** is to use **generator-thing**.

## Related

<dl>
  <dt>
  <a href="https://elioway.gitlab.io/eliothing/dna">dna</a>
</dt>
  <dd>Django models, views, forms, and templates, built out of schema.org "Things", the <strong>elioWay</strong>.</dd>
  <dt>
  <a href="https://elioway.gitlab.io/eliothing/generator-thing">generator-thing</a>
</dt>
  <dd>A yeoman generator for <strong>dna</strong> projects the <strong>elioWay</strong></dd>
  <dt>
  <a href="https://elioway.gitlab.io/eliothing/genes">genes</a>
</dt>
  <dd>Django templates, custom made for Schema.org Things, the <strong>elioWay</strong>.</dd>
  <dt>
  <a href="https://elioway.gitlab.io/eliosin">elioSin</a>
</dt>
  <dd>A classless, wireframing CSS framework built the <strong>elioWay</strong> with <strong>elioThing</strong> projects in mind.</dd>
</dl>
