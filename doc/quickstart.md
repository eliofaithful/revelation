# frankenstein Quickstart

- [Prerequisites](./prerequisites)

Please see **dna** QuickStart which describes how this application was created.

- [thing Quickstart](//docs.elioway/./eliothing/thing/doc/quickstart)

## Nutshell

```
cd frankenstein
virtualenv --python=python3 venv-frankenstein
pip install -e .
./init_frankenstein.sh
```
